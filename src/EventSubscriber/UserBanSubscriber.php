<?php

namespace Drupal\user_ban\EventSubscriber;

use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user_ban\Event\UserBanEvent;
use Drupal\user_ban\Event\UserUnbanEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * User ban event subscriber.
 */
class UserBanSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * User ban log channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(MailManagerInterface $mail_manager, LoggerInterface $logger) {
    $this->mailManager = $mail_manager;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'user_ban.ban' => ['onUserBan'],
      'user_ban.unban' => ['onUserUnban'],
    ];
  }

  /**
   * Log the ban and inform user about the ban.
   *
   * @param \Drupal\user_ban\Event\UserBanEvent $event
   */
  public function onUserBan(UserBanEvent $event) {
    $this->logger->notice($this->t('User @user (@uid) was banned.', [
      '@user' => $event->userBan->getBannedUser()->getDisplayName(),
      '@uid' => $event->userBan->getBannedUser()->id(),
    ]));

    $this->mailManager->mail(
      'user_ban',
      'user_ban',
      $event->userBan->getBannedUser()->getEmail(),
      \Drupal::languageManager()->getCurrentLanguage()->getId(),
      [
        'from' => \Drupal::config('system.site')->get('mail'),
        'subject' => $event->userBan->getSubject(),
        'body' => $event->userBan->getMessage(),
      ]
    );
  }

  /**
   * Log that the user was unbanned.
   *
   * @param \Drupal\user_ban\Event\UserUnbanEvent $event
   */
  public function onUserUnban(UserUnbanEvent $event) {
    $this->logger->notice($this->t('User @user (@uid) was reactivated.', [
      '@user' => $event->user->getDisplayName(),
      '@uid' => $event->user->id(),
    ]));
  }

}
