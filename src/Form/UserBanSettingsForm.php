<?php

namespace Drupal\user_ban\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for user ban.
 */
class UserBanSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_ban_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['user_ban.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['settings'] = [
      '#markup' => $this->t('Settings form for n user ban entity type.'),
    ];

    $form['unban_limit_on_cron'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Maximum number of users to unban on cron'),
      '#default_value' => $this->config('user_ban.settings')->get('unban_limit_on_cron'),
      '#description' => $this->t('If this is set to 0 user bans won\'t be lifted automatically.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('user_ban.settings')
      ->set('unban_limit_on_cron', $values['unban_limit_on_cron'])
      ->save();

    $this->messenger()->addStatus($this->t('The user ban configuration has been updated.'));
  }

}
