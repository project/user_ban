<?php

namespace Drupal\user_ban\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\user\UserInterface;
use Drupal\user_ban\UserBanService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Meta message templates form.
 */
class UnbanForm extends FormBase {

  /**
   * The user ban service.
   *
   * @var \Drupal\user_ban\UserBanService
   */
  protected $userBanService;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The controller constructor.
   *
   * @param \Drupal\user_ban\UserBanService $user_ban_service
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(UserBanService $user_ban_service, RouteMatchInterface $route_match, MessengerInterface $messenger) {
    $this->userBanService = $user_ban_service;
    $this->routeMatch = $route_match;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user_ban.service'),
      $container->get('current_route_match'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_unban_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var UserInterface $user */
    $user = $this->routeMatch->getParameter('user');

    $form['unban_description'] = [
      '#theme' => 'item_list',
      '#title' => $this->t('The following user bans will be lifted:'),
      '#items' => $this->userBanService->getUserBans($user),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' =>  $this->t('Unban'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var UserInterface $user */
    $user = $this->routeMatch->getParameter('user');
    $this->userBanService->unbanUser($user);
    $this->messenger->addStatus($this->t('All bans for the user @user were lifted.', ['@user' => $user->getDisplayName()]));
    $form_state->setRedirect('entity.user.canonical', ['user' => $user->id()]);
  }

}
