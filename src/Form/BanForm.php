<?php

namespace Drupal\user_ban\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\user\UserInterface;
use Drupal\user_ban\UserBanService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Meta message templates form.
 */
class BanForm extends FormBase {

  /**
   * The user ban service.
   *
   * @var \Drupal\user_ban\UserBanService
   */
  protected $userBanService;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The controller constructor.
   *
   * @param \Drupal\user_ban\UserBanService $user_ban_service
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(UserBanService $user_ban_service, RouteMatchInterface $route_match, MessengerInterface $messenger) {
    $this->userBanService = $user_ban_service;
    $this->routeMatch = $route_match;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user_ban.service'),
      $container->get('current_route_match'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_ban_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['time_period'] = [
      '#type' => 'number',
      '#min' => 1,
      '#field_suffix' => $this->t('Days'),
      '#title' => $this->t('Time period'),
      '#description' => $this->t('The time period the user shall be banned (empty means the user ban won\'t be lifted)'),
    ];

    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#maxlength' => 255,
      '#description' => $this->t('The subject why the user was banned.'),
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#description' => $this->t('A message to the user explaining why he was banned.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' =>  $this->t('Ban'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var UserInterface $user */
    $user = $this->routeMatch->getParameter('user');

    $unban_time = NULL;
    if ($time_period = $form_state->getValue('time_period')) {
      $unban_time = time() + $time_period * 60 * 60 * 24;
    }
    $this->userBanService->banUser($user, $unban_time, $form_state->getValue('subject'), $form_state->getValue('message'));
    $this->messenger->addStatus($this->t('The user @user was banned.', ['@user' => $user->getDisplayName()]));
    $form_state->setRedirect('entity.user.canonical', ['user' => $user->id()]);
  }

}
