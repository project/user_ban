<?php

namespace Drupal\user_ban;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface defining a user ban entity type.
 */
interface UserBanInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the user ban creation timestamp.
   *
   * @return int
   *   Creation timestamp of the user ban.
   */
  public function getCreatedTime();

  /**
   * Sets the user ban creation timestamp.
   *
   * @param int $timestamp
   *   The user ban creation timestamp.
   *
   * @return \Drupal\user_ban\UserBanInterface
   *   The called user ban entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Get the banned user.
   *
   * @return ?UserInterface
   */
  public function getBannedUser(): ?UserInterface;

  /**
   * Gets the user unban timestamp.
   *
   * @return ?int
   *   The time the user ban will be lifted.
   */
  public function getUnbanTime(): ?int;

  /**
   * Gets the subject the user was banned.
   *
   * @return string
   *   The subject.
   */
  public function getSubject(): string;

  /**
   * Gets the full subject the user was banned.
   *
   * @return string
   *   The subject.
   */
  public function getMessage(): string;

}
