<?php

namespace Drupal\user_ban\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\user_ban\UserBanInterface;

/**
 * Event that is dispatched when a user is banned.
 */
class UserBanEvent extends Event {

  const EVENT_NAME = 'user_ban.ban';

  /**
   * The user ban entity.
   *
   * @var \Drupal\user_ban\UserBanInterface
   */
  public $userBan;

  /**
   * Constructs a certificate created Event object.
   *
   * @param \Drupal\user_ban\UserBanInterface $user_ban
   *   The user ban entity.
   */
  public function __construct(UserBanInterface $user_ban) {
    $this->userBan = $user_ban;
  }

}
