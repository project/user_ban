<?php

namespace Drupal\user_ban\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\user\UserInterface;

/**
 * Event that is dispatched when a user is unbanned.
 */
class UserUnbanEvent extends Event {

  const EVENT_NAME = 'user_ban.unban';

  /**
   * The user entity.
   *
   * @var \Drupal\user_ban\UserBanInterface
   */
  public $user;

  /**
   * Constructs a certificate created Event object.
   *
   * @param \Drupal\user_ban\UserBanInterface $user
   *   The user entity.
   */
  public function __construct(UserInterface $user) {
    $this->user = $user;
  }

}
