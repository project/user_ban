<?php

namespace Drupal\user_ban;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\UserInterface;
use Drupal\user_ban\Event\UserBanEvent;
use Drupal\user_ban\Event\UserUnbanEvent;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * UserBanService service.
 */
class UserBanService {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a UserBanService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Check if a user is banned.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function userIsBanned(UserInterface $user): bool {
    return !empty($this->getUserBans($user));
  }

  /**
   * Get user bans.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return \Drupal\user_ban\UserBanInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getUserBans(UserInterface $user): array {
    return $this->entityTypeManager->getStorage('user_ban')->loadByProperties(['banned_user' => $user->id()]);
  }

  /**
   * Ban a user
   *
   * @param \Drupal\user\UserInterface $user
   * @param int|null $unban_time
   * @param string $subject
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function banUser(UserInterface $user, int $unban_time = NULL, string $subject = '', string $message = ''): void {
    $user_ban = $this->entityTypeManager->getStorage('user_ban')->create([
      'banned_user' => $user->id(),
      'unban_time' => $unban_time,
      'subject' => $subject,
      'message' => $message,
    ]);
    $user_ban->save();

    $user->block();
    $user->save();

    $event = new UserBanEvent($user_ban);
    $this->eventDispatcher->dispatch($event, UserBanEvent::EVENT_NAME);
  }

  /**
   * Ban a user
   *
   * @param \Drupal\user\UserInterface $user
   * @param $unban_time
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function unbanUser(UserInterface $user): void {
    foreach ($this->getUserBans($user) as $user_ban) {
      $user_ban->delete();
    }

    $this->reactivateUser($user);
  }

  /**
   * Lift a user ban
   *
   * @param \Drupal\user\UserInterface $user
   * @param $unban_time
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function liftUserBan(UserBanInterface $user_ban): void {
    if ($user = $user_ban->getBannedUser()) {
      $user_ban->delete();

      if (!$this->userIsBanned($user)) {
        $this->reactivateUser($user);
      }
    }
  }

  /**
   * Reactivate a user after bans were lifted.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function reactivateUser(UserInterface $user) {
    $user->activate();
    $user->save();

    $event = new UserUnbanEvent($user);
    $this->eventDispatcher->dispatch($event, UserUnbanEvent::EVENT_NAME);
  }



}
